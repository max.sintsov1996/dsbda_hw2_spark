Build and deploy manual
=======================

## Required tools

- jdk (version 7 or later)
- maven
- docker
- git

## Preparation

Download spark binaries (required only to submit job)

Preferably use [2.2.0-bin-hadoop2.7 version](https://archive.apache.org/dist/spark/spark-2.2.0/spark-2.2.0-bin-hadoop2.7.tgz)

### Install spark using following commands

```
cd /opt
wget https://archive.apache.org/dist/spark/spark-2.2.0/spark-2.2.0-bin-hadoop2.7.tgz
tar -xzf spark-2.2.0-bin-hadoop2.7.tgz
ln -s /opt/spark-2.2.0-bin-hadoop2.7  /opt/spark
export SPARK_HOME=/opt/spark
export PATH=$SPARK_HOME/bin:$PATH
```

### Download flume binaries 

referably use [apache-flume-1.9.0-bin version](https://apache-mirror.rbc.ru/pub/apache/flume/1.9.0/apache-flume-1.9.0-bin.tar.gz)

Install flume using following commands

```
cd /opt
wget https://apache-mirror.rbc.ru/pub/apache/flume/1.9.0/apache-flume-1.9.0-bin.tar.gz
tar -xzf apache-flume-1.9.0-bin.tar.gz
ln -s /opt/apache-flume-1.9.0-bin  /opt/flume
export FLUME_HOME=/opt/flume/bin
export PATH=$PATH:$FLUME_HOME/bin
```
Add the following JARs to Flume’s classpath

[Custom sink JAR](http://search.maven.org/remotecontent?filepath=org/apache/spark/spark-streaming-flume-sink_2.11/2.1.0/spark-streaming-flume-sink_2.11-2.1.0.jar)
[Scala library JAR](http://search.maven.org/remotecontent?filepath=org/scala-lang/scala-library/2.11.7/scala-library-2.11.7.jar)
[Commons Lang 3 JAR](http://search.maven.org/remotecontent?filepath=org/apache/commons/commons-lang3/3.5/commons-lang3-3.5.jar)


### Install python3 PySpark module version 2.2.1 and Pytest 

```
pip3 install 'pyspark==2.2.1'
```
```
pip3 install pytest
```

### Download project sources

```
git https://gitlab.com/max.sintsov1996/dsbda_hw2_spark.git
cd dsbda_hw2_spark
```

## Start system components

### Start flume
```
flume-ng agent --conf conf --conf-file dsbda_hw2_spark.conf --name a1 -Dflume.root.logger=INFO,console
```

### Start spark cluster

Starting spark master

```
cd /opt/spark
./sbin/start-master.sh
```

Starting spark slave

```
cd /opt/spark
./sbin/start-slave.sh <master-spark-URL>
```

## Deploy

### Prepare input data

### Generate test data using provided script

```
scripts/generate_data.sh 300 > test_data/data
```

Alternatively : use your logs in the following format

```
date hostname process: [<priority>] Message
#Examples:
#Oct 28 8:41:42 debian generator: <debug> Something happened!
#Oct 28 8:41:42 debian generator: Something happened!
```
### Start flume

```
scripts/start-flume.sh
```

### Submit spark job

```
scripts/submit.sh
```
### Send data to flume

```
scripts/sand_data.sh
```
## Check results

```
hdfs dfs -ls /user/root/linux_logs
```
