#!/bin/bash
rows_amount=$1
./generate_data.sh "$(($rows_amount))" > ../data/data_for_nc
netcat 127.0.0.1 44444 <../data/data_for_nc
yes | rm ../data/data_for_nc
