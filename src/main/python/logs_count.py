from __future__ import print_function

import sys
import re

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.flume import FlumeUtils


# Priority and message matching dictionary
priority_dictionary = {
    '<debug>': 7,
    '<info>': 6,
    'Something': 6,
    '<notice>': 5,
    '<warning>': 4,
    '<warn>': 4,
    '<err>': 3,
    '<error>': 3,
    '<crit>': 2,
    '<alert>': 1,
    '<emerg>': 0,
    '<panic>': 0
}

# Сonverting the string to the required form
# from "Nov 04 21:2:4 max-Lenovo-IdeaPad generator: <alert> Something happened!"
# to Nov 04 21:00:00 1
def transform_row(row):
    words = re.split('[ :]', row)

    month  = words[0]
    day    = words[1]
    hour   = words[2]
    status = words[8]

    priority = priority_dictionary.get(status, "malformed")

    if(priority == "malformed"):
        return "malformed"

    # align lines
    if (int(hour) < 10):
        out = month+" " + day+" " + hour + ":00:00  " + str(priority)
    else:
        out = month+" "+day+" "+hour+":00:00 "+str(priority)

    return out


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: log_count.py <hostname> <port>", file=sys.stderr)
        exit(-1)

    sc = SparkContext(appName="PythonStreamingHDFSWordCount")

    # Configure Spark to read data every 5 seconds
    ssc = StreamingContext(sc, 5)

    hostname, port = sys.argv[1:]

    # Flume connection
    kvs = FlumeUtils.createStream(ssc, hostname, int(port))
    lines = kvs.map(lambda x: x[1])

    # Transform rows, reduce by key and count similar
    lines = ssc.textFileStream(sys.argv[1])
    counts = lines.flatMap(lambda line: line.split("/n"))\
                  .map(lambda x: (transform_row(x), 1))\
                  .reduceByKey(lambda a, b: a+b)

    # Write results to HDFS
    counts.saveAsTextFiles("hdfs://127.0.0.1:9000/user/root/logs/", "txt")

    ssc.start()
    ssc.awaitTermination()
