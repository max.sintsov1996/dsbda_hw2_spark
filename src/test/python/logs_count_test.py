from __future__ import print_function

import sys

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.flume import FlumeUtils

import re
import os
import pytest

# Priority and message matching dictionary
priority_dictionary = {
    '<debug>': 7,
    '<info>': 6,
    'Something': 6,
    '<notice>': 5,
    '<warning>': 4,
    '<warn>': 4,
    '<err>': 3,
    '<error>': 3,
    '<crit>': 2,
    '<alert>': 1,
    '<emerg>': 0,
    '<panic>': 0
}


# Сonverting the string to the required form
# from "Nov 04 21:2:4 max-Lenovo-IdeaPad generator: <alert> Something happened!"
# to Nov 04 21:00:00 1
def transform_row(row):
    words = re.split('[ :]', row)

    month  = words[0]
    day    = words[1]
    hour   = words[2]
    status = words[8]

    priority = priority_dictionary.get(status, "malformed")

    if(priority == "malformed"):
        return "malformed"

    # выравнивание строк
    if (int(hour) < 10):
        out = month+" " + day+" " + hour + ":00:00  " + str(priority)
    else:
        out = month+" "+day+" "+hour+":00:00 "+str(priority)

    return out


def test_transform_row():
    row = "Nov 04 21:2:4 max-Lenovo-IdeaPad generator: <alert> Something happened!"
    assert transform_row(row) == "Nov 04 21:00:00 1"


def test_transform_row_malformed():
    row = "Nov 04 21:2:4 max-Lenovo-IdeaPad generator: <malformed> Something happened!"
    assert transform_row(row) == "malformed"


if __name__ == "__main__":

    sc = SparkContext(appName="PythonStreamingHDFSWordCount")
    
    # Configure Spark to read data every 5 seconds
    ssc = StreamingContext(sc, 5)

    lines = ssc.textFileStream("file://"+os.getcwd())
    # Transform rows, reduce by key and count similar
    lines = ssc.textFileStream(os.getcwd()+"/test_data"+"/")
    counts = lines.flatMap(lambda line: line.split("/n"))\
                  .map(lambda x: (transform_row(x), 1))\
                  .reduceByKey(lambda a, b: a+b)

    counts.saveAsTextFiles("file://"+os.getcwd()+"/", "txt")

    ssc.start()
    ssc.awaitTermination()



